#!/usr/bin/env python
"""
 Blinks an LED on digital pin 9
 in morse code a message of your choosing
"""

from Arduino import Arduino
import time
import argparse


# Globals that should be left alone.
VERSION = "1.5"
DIT = "0"
DAH = "1"

MORSE_MAP = {
    "a": "01",
    "b": "1000",
    "c": "1010",
    "d": "100",
    "e": "0",
    "f": "0010",
    "g": "110",
    "h": "0000",
    "i": "00",
    "j": "0111",
    "k": "101",
    "l": "0100",
    "m": "11",
    "n": "10",
    "o": "111",
    "p": "0110",
    "q": "1101",
    "r": "010",
    "s": "000",
    "t": "1",
    "u": "001",
    "v": "0001",
    "w": "011",
    "x": "1001",
    "y": "1011",
    "z": "1100",
    "1": "01111",
    "2": "00111",
    "3": "00011",
    "4": "00001",
    "5": "00000",
    "6": "10000",
    "7": "11000",
    "8": "11100",
    "9": "11110",
    "0": "11111",
}

time_unit_init = 0.25  # In seconds
timings = {
    "time_unit": time_unit_init,
    "dit_time": time_unit_init,
    "dah_time": time_unit_init * 3.0,
    "intra_char_time": time_unit_init,
    "inter_char_time": time_unit_init * 3.0,
    "word_space_time": time_unit_init * 7.0,
}
led_pin = 9


def main(message, baud_rate, serial_port):
    board = Arduino(baud=baud_rate, port=serial_port)
    print(f"Version: {VERSION}")
    print(f"Out is set to use pin {led_pin}")
    print("")

    board.pinMode(led_pin, "OUTPUT")

    while True:
        for item in list(message.lower()):
            # Configure timers each loop in case the user changes speed mid-run.
            config_timings()

            # Check to see if the message char is in the MORSE_MAP.
            if item in MORSE_MAP:
                print(f"Message Char: {item}")
                decode_char(board=board, code=MORSE_MAP[item])
                time.sleep(timings["inter_char_time"])
                print("")
            else:
                print("\n")
                time.sleep(timings["word_space_time"])
        time.sleep(timings["word_space_time"])


def dit_or_dah(value):
    if value == DIT:
        return "DIT"
    return "DAH"


def decode_char(board, code=""):
    code_length = len(code)
    for i, item in enumerate(list(code)):
        print(f"\tMorse: {dit_or_dah(item)}")
        on_time = 0
        if item == DIT:
            on_time = timings["dit_time"]
        elif item == DAH:
            on_time = timings["dah_time"]
        send_to_led(board=board, on_duration=on_time)
        if i < code_length:
            time.sleep(timings["intra_char_time"])


def send_to_led(board, on_duration=0):
    board.digitalWrite(led_pin, "HIGH")
    time.sleep(on_duration)
    board.digitalWrite(led_pin, "LOW")


def config_timings():
    time_unit = get_time_unit()
    timings["time_unit"] = time_unit
    timings["dit_time"] = time_unit * 1
    timings["dah_time"] = time_unit * 3
    timings["intra_char_time"] = time_unit
    timings["inter_char_time"] = time_unit
    timings["word_space_time"] = time_unit * 7


def get_time_unit():
    return (
        time_unit_init
    )  # I plan on having this on a variable resistor so users can change the overall speed.


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert message into Morse")
    parser.add_argument(
        "-m",
        "--message",
        action="store",
        dest="message",
        type=str,
        help="Message to be Morse encoded",
        default="Hello world",
    )
    parser.add_argument(
        "-t",
        "--timing",
        action="store",
        dest="time_unit_init",
        type=float,
        help="Initial time for timing calculations (in seconds).",
        default=time_unit_init,
    )
    parser.add_argument(
        "-l",
        "--led_pin",
        action="store",
        dest="led_pin",
        type=int,
        help="Digital pin where LED is wired.",
        default=led_pin,
    )
    parser.add_argument(
        "-b",
        "--baud",
        action="store",
        dest="baud_rate",
        type=str,
        help="Baud rate on serial port.",
        default="115200",
    )
    parser.add_argument(
        "-p",
        "--serial_port",
        action="store",
        dest="serial_port",
        type=str,
        help="Serial port where Arduino is cabled.",
        default="/dev/ttyACM0",
    )
    args = parser.parse_args()
    timings = {
        "time_unit": args.time_unit_init,
        "dit_time": args.time_unit_init,
        "dah_time": args.time_unit_init * 3.0,
        "intra_char_time": args.time_unit_init,
        "inter_char_time": args.time_unit_init * 3.0,
        "word_space_time": args.time_unit_init * 7.0,
    }
    led_pin = args.led_pin

    main(message=args.message,
         baud_rate=args.baud_rate,
         serial_port=args.serial_port,
         )
