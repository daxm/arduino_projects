/*
  Morse
  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products
*/

// User editable values
String message = String("Hello world");
unsigned int time_unit_init = 250;  // In milliseconds
int red_led_pin = 9;

// Globals that should be left alone.
#define DIT 0
#define DAH 1
#define VERSION 1.5
char MORSE_MAP[72][6] = {
  "a", "01",
  "b", "1000",
  "c", "1010",
  "d", "100",
  "e", "0",
  "f", "0010",
  "g", "110",
  "h", "0000",
  "i", "00",
  "j", "0111",
  "k", "101",
  "l", "0100",
  "m", "11",
  "n", "10",
  "o", "111",
  "p", "0110",
  "q", "1101",
  "r", "010",
  "s", "000",
  "t", "1",
  "u", "001",
  "v", "0001",
  "w", "011",
  "x", "1001",
  "y", "1011",
  "z", "1100",
  "1", "01111",
  "2", "00111",
  "3", "00011",
  "4", "00001",
  "5", "00000",
  "6", "10000",
  "7", "11000",
  "8", "11100",
  "9", "11110",
  "0", "11111",
};
unsigned int MORSE_MAP_SIZE = 72;


// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(red_led_pin, OUTPUT);

  // Set up serial so you can see debug output.
  Serial.begin(115200);
  Serial.println("Morse version: " + String(VERSION));
  Serial.println("Output is set to use pin " + String(red_led_pin));
}


// the loop function runs over and over again forever
void loop() {
  char cArray[message.length() + 1];
  boolean match = false;
  char messageChar;
  char morseChar;
  char morseBinary;
  // Configure timing.  All timings are sourced by time_unit value.
  unsigned int time_unit = get_time_unit();
  unsigned int dit_time = time_unit * 1;
  unsigned int dah_time = time_unit * 3;
  unsigned int intra_char_time = dit_time;
  unsigned int inter_char_time = dah_time;
  unsigned int word_space_time = time_unit * 7;

  message.toLowerCase();
  message.toCharArray(cArray, message.length() + 1);

  // Loop through every character of the message.
  for (unsigned int i = 0; i < message.length(); i++) {
    // Compare each character of the message for a match in the MORSE_MAP.
    match = false;
    for (unsigned int j = 0; j < MORSE_MAP_SIZE; j = j + 2) {
      messageChar = cArray[i];
      morseChar = MORSE_MAP[j];
      Serial.println("messageChar: " + String(messageChar) + " morseChar: " + String(morseChar));
      /* Broken
      if (strcmp(morseChar,messageChar)) {
        morseBinary = MORSE_MAP[j + 1];
        char cLetter[morseBinary.length() + 1];
        morseBinary.toCharArray(cLetter, morseBinary.length() + 1);
        Serial.print("Message char " + messageChar + " maps to " + morseChar + " which is ");
        for (unsigned int k = 0; k < morseBinary.length(); j++) {
          Serial.print(cLetter[k]);
        }
        Serial.println();
      }
    */
    }
  }


}


unsigned int get_time_unit() {
  // Someday I might have a timing adjuster to modulate the speed of the morse.  For now just return hard coded value.
  return time_unit_init;
}


void send_to_led(unsigned int char_type, unsigned int char_timing) {
  digitalWrite(red_led_pin, HIGH);  // turn the LED on (HIGH is the voltage level)
  delay(char_timing);            // Leave LED on for this amount of time.
  digitalWrite(red_led_pin, LOW);
}
