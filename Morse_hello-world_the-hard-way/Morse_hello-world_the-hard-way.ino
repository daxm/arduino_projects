/*
  Morse

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products
*/

// User editable values
String message = String("Hello world");

unsigned int DIT = 0;
unsigned int DAH = 1;
unsigned int TIME_UNIT_INIT = 500;  // In milliseconds


// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}


// the loop function runs over and over again forever
void loop() {
  // Configure timing.  All timings are sourced by time_unit value.
  unsigned int time_unit;
  time_unit = get_time_unit();
  unsigned int dit_time = time_unit * 1;
  unsigned int dah_time = time_unit * 3;
  unsigned int intra_char_time = dit_time;
  unsigned int inter_char_time = dah_time;
  unsigned int word_space_time = time_unit * 7;

  // "hello world" the hard way:
  // "h"
  send_to_led(DIT, dit_time);
  delay(intra_char_time);
  send_to_led(DIT, dit_time);
  delay(intra_char_time);
  send_to_led(DIT, dit_time);
  delay(intra_char_time);
  send_to_led(DIT, dit_time);

  // letter space time out.
  delay(inter_char_time);           // wait between letters.

  // "e"
  send_to_led(DIT, dit_time);

  // letter space time out.
  delay(inter_char_time);           // wait between letters.

  // "l"
  send_to_led(DIT, dit_time);
  delay(intra_char_time);
  send_to_led(DAH, dah_time);
  delay(intra_char_time);
  send_to_led(DIT, dit_time);
  delay(intra_char_time);
  send_to_led(DIT, dit_time);

  // letter space time out.
  delay(inter_char_time);           // wait between letters.

  // "l"
  send_to_led(DIT, dit_time);
  delay(intra_char_time);
  send_to_led(DAH, dah_time);
  delay(intra_char_time);
  send_to_led(DIT, dit_time);
  delay(intra_char_time);
  send_to_led(DIT, dit_time);

  // letter space time out.
  delay(inter_char_time);           // wait between letters.

  // "o"
  send_to_led(DAH, dah_time);
  delay(intra_char_time);
  send_to_led(DAH, dah_time);
  delay(intra_char_time);
  send_to_led(DAH, dah_time);

  // letter space time out.
  delay(word_space_time);           // wait between words.

  // "w"
  send_to_led(DIT, dit_time);
  delay(intra_char_time);
  send_to_led(DAH, dah_time);
  delay(intra_char_time);
  send_to_led(DAH, dah_time);

  // letter space time out.
  delay(inter_char_time);           // wait between letters.

  // "o"
  send_to_led(DAH, dah_time);
  delay(intra_char_time);
  send_to_led(DAH, dah_time);
  delay(intra_char_time);
  send_to_led(DAH, dah_time);

  // letter space time out.
  delay(inter_char_time);           // wait between letters.

  // "r"
  send_to_led(DIT, dit_time);
  delay(intra_char_time);
  send_to_led(DAH, dah_time);
  delay(intra_char_time);
  send_to_led(DIT, dit_time);
  delay(intra_char_time);

  // letter space time out.
  delay(inter_char_time);           // wait between letters.

  // "l"
  send_to_led(DIT, dit_time);
  delay(intra_char_time);
  send_to_led(DAH, dah_time);
  delay(intra_char_time);
  send_to_led(DIT, dit_time);
  delay(intra_char_time);
  send_to_led(DIT, dit_time);

  // letter space time out.
  delay(inter_char_time);           // wait between letters.

  // "d"
  send_to_led(DAH, dah_time);
  delay(intra_char_time);
  send_to_led(DIT, dit_time);
  delay(intra_char_time);
  send_to_led(DIT, dit_time);
  delay(intra_char_time);

  // letter space time out.
  delay(word_space_time);           // wait between words.
}


unsigned int get_time_unit() {
  // Someday I might have a timing adjuster to modulate the speed of the morse.  For now just return hard coded value.
  return TIME_UNIT_INIT;
}


void send_to_led(unsigned int char_type, unsigned int char_timing) {
  digitalWrite(LED_BUILTIN, HIGH);  // turn the LED on (HIGH is the voltage level)
  delay(char_timing);            // Leave LED on for this amount of time.
  digitalWrite(LED_BUILTIN, LOW);
}
