# arduino_projects
This repo is a store of all my Arduino tests and projects.  I'm brand new to Arduino so don't
expect miracle code here.  :-)

## Morse
My first project is to use the base knowledge in the BLINK default project to build a simple
morse code program that displays "hello world" in morse code.  The 
"Morse_hello-world_the-hard-way" directory is a working example of this.  However, my ultimate 
goal is to make the "Morse" directory more programatically correct way.

